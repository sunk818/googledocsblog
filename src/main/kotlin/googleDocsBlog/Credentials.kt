package googleDocsBlog

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import com.google.api.services.drive.DriveScopes

private val SCOPES = listOf(DriveScopes.DRIVE)

object CredentialsReader {
    fun getCredentials(): Credential? {
        val credentialJson = this::class.java.getResourceAsStream("/credentials.json")
        return GoogleCredential.fromStream(credentialJson)
                .createScoped(SCOPES)
    }
}