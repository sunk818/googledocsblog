package googleDocsBlog

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import java.io.File

fun cleanUpBlogFolder(blogFolder: String) {
    // Files that should always stay
    val safeFiles = setOf("style.css")
    File(blogFolder).walkTopDown().forEach {
        if (safeFiles.contains(it.name)) return
        else it.delete()
    }
}

fun main(args: Array<String>) {
    // Allows a separate blog directory for deployment
    val blogDirectory = if (!args.isEmpty()) args[0] else "blog"

    val httpTransport = GoogleNetHttpTransport.newTrustedTransport()
    val credentials = CredentialsReader.getCredentials()

    if (credentials != null) {
        val documents = getDocumentList(httpTransport, credentials)
        val documentNames = documents?.map { it.name }

        if (documents != null) {
            cleanUpBlogFolder(blogDirectory)
        }

        val indexTemplate = generateIndexHtmlTemplate(documentNames)
        val indexHtml = renderHtml(indexTemplate)
        writeFile("${blogDirectory}/index.html", indexHtml)

        documents?.forEach {
            val document = getDocument(it.id, httpTransport, credentials)

            val htmlTemplate = generateHtmlTemplate(it.name, document, documentNames)
            val html = renderHtml(htmlTemplate)
            writeFile("${blogDirectory}/${it.name.replace(' ', '-')}.html", html)
        }
    }
}
